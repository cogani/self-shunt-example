import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

public class LibrarianQueryTest implements IDatabase{
    private boolean wasCalled;

    public void query(String query){
     assertEquals("select * from Accounts", query);
        wasCalled = true;
    }

    @Test
    public void testQuery(){
        Librarian l = new Librarian(this);
        l.fetchAccounts();
        assertTrue(wasCalled);

    }
}
